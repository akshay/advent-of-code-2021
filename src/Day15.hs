module Day15 where

import Data.Foldable (foldl')
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set

day15_1 :: IO ()
day15_1 = do
  m <- readInput
  let maxX = maximum . map fst $ Map.keys m
      maxY = maximum . map snd $ Map.keys m
      (shortestRisk, shortestTrail) = findShortestPathDFS m (maxX, maxY) (0, 0)
  putStrLn $ renderPaths shortestTrail m
  putStrLn $ "Shortest Risk: " <> show shortestRisk

day15_2 :: IO ()
day15_2 = do
  t <- readInput
  let rightRepeated = foldr (Map.union . repeatRight t) t [4, 3, 2, 1]
      m = foldr (Map.union . repeatDown rightRepeated) rightRepeated [1 .. 4]
      maxX = maximum . map fst $ Map.keys m
      maxY = maximum . map snd $ Map.keys m
      (shortestRisk, shortestTrail, exploredTrail) = findShortestPathBFS m (maxX, maxY) (0, 0)
  putStrLn $ renderPaths shortestTrail m
  putStrLn $ renderPaths exploredTrail m
  putStrLn $ "Shortest Risk: " <> show shortestRisk

-- >>> repeatRight (Map.fromList [((1,0), 1)]) 1
-- fromList [((3,0),2)]
--
-- >>> repeatRight (Map.fromList [((1,0), 9)]) 1
-- fromList [((3,0),1)]
--
-- >>> repeatRight (Map.fromList [((0,0), 1), ((1,0), 9)]) 2
-- fromList [((4,0),3),((5,0),2)]
repeatRight :: RiskMap Int -> Int -> RiskMap Int
repeatRight tile n =
  let maxX = maximum . map fst $ Map.keys tile
   in Map.foldrWithKey
        ( \(x, y) r ->
            let newX = x + ((maxX + 1) * n)
                newR = ((r + n - 1) `mod` 9) + 1
             in Map.insert (newX, y) newR
        )
        mempty
        tile

repeatDown :: RiskMap Int -> Int -> RiskMap Int
repeatDown tile n =
  let maxY = maximum . map snd $ Map.keys tile
   in Map.foldrWithKey
        ( \(x, y) r ->
            let newY = y + (maxY + 1) * n
                newR = ((r + n - 1) `mod` 9) + 1
             in Map.insert (x, newY) newR
        )
        mempty
        tile

findShortestPathBFS :: RiskMap Int -> Coords -> Coords -> (Int, Trail, Trail)
findShortestPathBFS m to from =
  let exploredMap = ignoreExplored $ bfs m to (Map.singleton from (0, Set.singleton from, False))
      (shortestRisk, shortestTrail) = Map.findWithDefault (error $ "Never reached: " <> show to <> " from: " <> show from) to exploredMap
   in (shortestRisk, shortestTrail, Map.keysSet exploredMap)

findShortestPathDFS :: RiskMap Int -> Coords -> Coords -> (Int, Trail)
findShortestPathDFS m to from =
  Map.findWithDefault (error $ "Never reached: " <> show to <> " from: " <> show from) to $
    dfs m to from 0 (Set.singleton from) mempty

type Trail = Set Coords

renderPaths :: Trail -> Map Coords Int -> String
renderPaths path riskMap =
  let maxX = maximum . map fst $ Map.keys riskMap
      maxY = maximum . map snd $ Map.keys riskMap
      showPoint c r = if Set.member c path then "\ESC[31m" <> show r <> "\ESC[0m" else show r
      line y = unwords $ map (\x -> showPoint (x, y) $ Map.findWithDefault 0 (x, y) riskMap) [0 .. maxX]
   in unlines $ map line [0 .. maxY]

unrenderPaths :: Map Coords Int -> String
unrenderPaths riskMap =
  let maxY = maximum . map snd $ Map.keys riskMap
   in concatMap (const "\ESC[1A\ESC[K") [0 .. (maxY + 1)]

-- | Depth first search
dfs :: RiskMap Int -> Coords -> Coords -> Int -> Trail -> Map Coords (Int, Trail) -> Map Coords (Int, Trail)
dfs individualRisk to from currentRisk trail leastTotalRisk
  | Map.member from leastTotalRisk && fst (leastTotalRisk Map.! from) <= currentRisk = leastTotalRisk
  | Map.member to leastTotalRisk && fst (leastTotalRisk Map.! to) <= (currentRisk + manhattan from to) = leastTotalRisk
  | otherwise =
    let newLeastTotalRisk = Map.insertWith smallerPath from (currentRisk, trail) leastTotalRisk
     in if to == from
          then -- Debug.traceShow ("Shortest", fst $ Map.findWithDefault (error "imposss") to newLeastTotalRisk) newLeastTotalRisk
            newLeastTotalRisk
          else
            let rd = neighbourSelection [right from, down from]
                lu = neighbourSelection [left from, up from]
             in foldl'
                  ( \t c ->
                      case Map.lookup c individualRisk of
                        Nothing -> t
                        Just risk -> dfs individualRisk to c (currentRisk + risk) (Set.insert c trail) t
                  )
                  newLeastTotalRisk
                  (rd <> lu)
  where
    neighbourSelection = List.sortOn (`Map.lookup` individualRisk) . filter (`notElem` trail)

smallerPath :: (Int, b) -> (Int, b) -> (Int, b)
smallerPath old new = if fst old <= fst new then old else new

manhattan :: Coords -> Coords -> Int
manhattan (x1, y1) (x2, y2) = abs (x2 - x1) + abs (y2 - y1)

-- | Breadth first search
bfs :: RiskMap Int -> Coords -> Map Coords (Int, Trail, Bool) -> Map Coords (Int, Trail, Bool)
bfs individualRisk to leastTotalRisk
  | Map.member to leastTotalRisk = leastTotalRisk
  | otherwise =
    let (nextTarget, nextTargetTrail, nextTargetRisk) =
          Map.foldrWithKey
            ( \c (r, t, explored) (minC, minT, minR) ->
                if not explored && r < minR
                  then (c, t, r)
                  else (minC, minT, minR)
            )
            ((0, 0), Set.fromList [], maxBound)
            leastTotalRisk
        neighbours = [right nextTarget, down nextTarget, up nextTarget, left nextTarget]
        markNextExplored = Map.adjust (\(r, t, _) -> (r, t, True)) nextTarget leastTotalRisk
        newLeastTotalRisk =
          foldr
            ( \n ->
                case Map.lookup n individualRisk of
                  Nothing -> id
                  Just r ->
                    Map.insertWith (\_ old -> old) n (nextTargetRisk + r, Set.insert n nextTargetTrail, False)
            )
            markNextExplored
            neighbours
     in -- if Map.size newLeastTotalRisk `mod` 100 == 0
        --     then Debug.traceShow ("Checkpoint: ", Map.size newLeastTotalRisk, "biggest", maximumBy (\(x1, y1) (x2, y2) -> compare (x1 + y1) (x2 + y2)) $ Map.keysSet newLeastTotalRisk, "exploring", nextTarget) $ bfs individualRisk to newLeastTotalRisk
        --     else bfs individualRisk to newLeastTotalRisk
        bfs individualRisk to newLeastTotalRisk

ignoreExplored :: Map Coords (Int, Trail, Bool) -> Map Coords (Int, Trail)
ignoreExplored = Map.map (\(i, t, _) -> (i, t))

right :: Coords -> Coords
right (x, y) = (x + 1, y)

down :: Coords -> Coords
down (x, y) = (x, y + 1)

left :: Coords -> Coords
left (x, y) = (x - 1, y)

up :: Coords -> Coords
up (x, y) = (x, y - 1)

type Coords = (Int, Int)

type RiskMap a = Map Coords a

readInput :: IO (RiskMap Int)
readInput = do
  ls <- lines <$> getContents
  pure . Map.fromList . concat $
    zipWith
      ( \l j ->
          zipWith
            (\c i -> ((i, j), read [c]))
            l
            [0 ..]
      )
      ls
      [0 ..]
