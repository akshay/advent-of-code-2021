module Day03 where

import Data.Bits
import Data.List (transpose)

day3_1 :: IO ()
day3_1 = do
  rows <- readInput
  let columns = transpose rows
      freqs = map calcFrequencies columns
      gammaBits = map (\(fs, ts) -> ts > fs) freqs
      gamma = interpretBools gammaBits
      epsilon = interpretBools $ map not gammaBits
      powerConsumption = gamma * epsilon
  putStrLn $ "Power consumption: " <> show powerConsumption

-- >>> calcFrequencies [True, False, True, True]
-- (1,3)
calcFrequencies :: [Bool] -> (Int, Int)
calcFrequencies = foldr (\b (fs, ts) -> if b then (fs, ts + 1) else (fs + 1, ts)) (0, 0)

-- >>> interpretBools [False, True, True, False]
-- 6
interpretBools :: [Bool] -> Int
interpretBools bs = foldr (\(b, i) acc -> if b then setBit acc i else acc) 0 $ zip (reverse bs) [0 ..]

day3_2 :: IO ()
day3_2 = do
  input <- readInput
  let o2 = interpretBools $ o2GenRating input
      co2 = interpretBools $ co2GenRating input
  putStrLn $ "Life support rating: " <> show (o2 * co2)

-- >>> input = parseInput "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010"
-- >>> interpretBools $ o2GenRating input
-- 23
o2GenRating :: [BinaryNumber] -> BinaryNumber
o2GenRating numbers = calcRating (<=) numbers 0

-- >>> input = parseInput "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010"
-- >>> interpretBools $ co2GenRating input
-- 10
co2GenRating :: [BinaryNumber] -> BinaryNumber
co2GenRating numbers = calcRating (>) numbers 0

calcRating :: (Int -> Int -> Bool) -> [BinaryNumber] -> Int -> BinaryNumber
calcRating _freqComparision [n] _ = n
calcRating freqComparision numbers pos =
  let bitsAtPos = transpose numbers !! pos
      (fsAtPos, tsAtPos) = calcFrequencies bitsAtPos
      desiredBitAtPos = freqComparision fsAtPos tsAtPos
      remainingNumbers = filter (\n -> n !! pos == desiredBitAtPos) numbers
   in calcRating freqComparision remainingNumbers (pos + 1)

type BinaryNumber = [Bool]

readInput :: IO [BinaryNumber]
readInput = do
  parseInput <$> getContents

parseInput :: String -> [BinaryNumber]
parseInput input =
  map (map readBit) (lines input)
  where
    readBit :: Char -> Bool
    readBit '0' = False
    readBit '1' = True
    readBit _ = error "Invalid Input"
