{-# LANGUAGE TupleSections #-}

module Day17 where

import Data.List (intercalate, scanl')
import Data.List.Extra (notNull)
import qualified Data.Set as Set
import Numeric.Extra (intToFloat)
import Text.ParserCombinators.ReadP

day17_1 :: IO ()
day17_1 = do
  (_, (tymin, tymax)) <- readInput
  let yVelsAndSteps = filter (\(_, steps) -> notNull steps) $ map (\vy -> (vy, tryY (tymin, tymax) vy)) [tymin .. (negate tymin)]
      maxYVel = maximum $ map fst yVelsAndSteps
      heighestPoint = (maxYVel * (maxYVel + 1)) `div` 2
  putStrLn $ "Heighest Point: " <> show heighestPoint

day17_2 :: IO ()
day17_2 = do
  ((txmin, txmax), (tymin, tymax)) <- readInput
  let yVelsAndSteps = filter (\(_, steps) -> notNull steps) $ map (\vy -> (vy, tryY (tymin, tymax) vy)) [tymin .. (negate tymin)]
      minXVel = ceiling $ (sqrt (intToFloat (1 + 8 * txmin)) - 1) / 2
      xVelsAndSteps = map (\vx -> (vx, tryX (txmin, txmax) vx)) [minXVel .. txmax]
      xyVels =
        Set.fromList $
          concatMap
            ( \(yVel, ysteps) ->
                map ((,yVel) . fst) $
                  filter
                    ( notNull
                        . takeWhile (`elem` ysteps)
                        . dropWhile (\s -> s < minimum ysteps)
                        . snd
                    )
                    xVelsAndSteps
            )
            yVelsAndSteps
  putStrLn $ "Distinct initial velocities: " <> show xyVels
  putStrLn $ "No. of Distinct initial velocities: " <> show (Set.size xyVels)

-- >>> tryY (-10, - 5) 8
-- [18]
--
-- >>> tryY (-10, -5) 1
-- [5,6]
tryY :: (Int, Int) -> Int -> [Int]
tryY (ymin, ymax) vy =
  let targetLandings =
        takeWhile (\((_, p), _) -> p >= ymin)
          . dropWhile (\((_, p), _) -> p > ymax)
          $ scanl' (\(vp, _) t -> (stepY vp, t)) ((vy, 0), 0) [1 ..]
   in snd <$> targetLandings

-- >>> tryX (20, 30) 20
-- [1]
--
-- >>> take 5 $ tryX (20, 30) 6
-- [5,6,7,8,9]
tryX :: (Int, Int) -> Int -> [Int]
tryX (xmin, xmax) vx =
  let targetLandings =
        takeWhile (\((_, p), _) -> p <= xmax)
          . dropWhile (\((_, p), _) -> p < xmin)
          $ scanl (\(vp, _) t -> (stepX vp, t)) ((vx, 0), 0) [1 ..]
   in snd <$> targetLandings

paint :: (TargetArea, TargetArea) -> [(Pos, Pos)] -> String
paint ((txmin, txmax), (tymin, tymax)) ps =
  let isTarget (x, y) = x >= txmin && x <= txmax && y >= tymin && y <= tymax
      maxY = maximum $ tymax : map snd ps
      minY = minimum $ tymin : map snd ps
      maxX = maximum $ txmax : map fst ps
      line y =
        intercalate "" $
          map
            ( \x ->
                if (x, y) `elem` ps
                  then "#"
                  else
                    if isTarget (x, y)
                      then "T"
                      else if (x, y) == (0, 0) then "S" else "."
            )
            [0 .. maxX]
   in unlines . map line $ reverse [minY .. maxY]

-- >>> animate (7,2) 7
-- [(7,2),(13,3),(18,3),(22,2),(25,0),(27,-3),(28,-7)]
animate :: (Velocity, Velocity) -> Int -> [(Pos, Pos)]
animate (vx, vy) n = zip (animateX vx n) (animateY vy n)

-- >>> animateY 5 11
-- [5,9,12,14,15,15,14,12,9,5,0]
animateY :: Velocity -> Int -> [Pos]
animateY vy steps = map (\n -> (negate (n * n) + 2 * vy * n + n) `div` 2) [1 .. steps]

-- >>> animateX 5 11
-- [5,9,12,14,15,15,15,15,15,15,15]
animateX :: Velocity -> Int -> [Pos]
animateX vx steps = map posX [1 .. steps]
  where
    posX n
      | n >= vx = (vx * (vx + 1)) `div` 2
      | otherwise = ((vx * (vx + 1)) `div` 2) - (((vx - n) * (vx - n + 1)) `div` 2)

stepY :: (Velocity, Pos) -> (Velocity, Pos)
stepY (v, y) = (v - 1, y + v)

stepX :: (Velocity, Pos) -> (Velocity, Pos)
stepX (v, x) =
  case v `compare` 0 of
    LT -> (v + 1, x + v)
    GT -> (v - 1, x + v)
    EQ -> (v, x)

type Pos = Int

type Velocity = Int

type TargetArea = (Int, Int)

readInput :: IO (TargetArea, TargetArea)
readInput = do
  inputStr <- getLine
  case readP_to_S (inputParser <* eof) inputStr of
    [] -> error "No Parse"
    [(i, "")] -> pure i
    xs -> error $ "Many parse: " <> show xs

-- >>> readP_to_S (inputParser <* eof) "target area: x=119..176, y=-141..-84"
-- [(((119,176),(-141,-84)),"")]
inputParser :: ReadP (TargetArea, TargetArea)
inputParser = do
  _ <- string "target area: "
  _ <- string "x="
  x <- rangeParser
  _ <- string ", y="
  y <- rangeParser
  pure (x, y)

-- >>> readP_to_S (rangeParser <* eof) "119..176"
-- [((119,176),"")]
rangeParser :: ReadP TargetArea
rangeParser = do
  x1 <- intParser
  _ <- string ".."
  x2 <- intParser
  pure (x1, x2)

-- >>> readP_to_S (intParser) "111.."
-- [(111,"..")]
--
-- >>> readP_to_S (intParser) "-111.."
-- [(-111,"..")]
intParser :: ReadP Int
intParser = readS_to_P reads
