module Day10 where

import Data.Either (lefts, rights)
import Data.Foldable (foldl')
import Data.List (sort)
import Data.Maybe (mapMaybe)

day10_1 :: IO ()
day10_1 = do
  input <- readInput
  let wrongClosings = lefts $ mapMaybe (parseLine "") input
      score = sum $ map scoreClosing wrongClosings
  putStrLn $ "Syntax error score: " <> show score

day10_2 :: IO ()
day10_2 = do
  input <- readInput
  let unclosedOpenings = rights $ mapMaybe (parseLine "") input
      score = middle $ map scoreCompletion unclosedOpenings
  putStrLn $ "Middle score: " <> show score

scoreClosing :: Char -> Int
scoreClosing ')' = 3
scoreClosing ']' = 57
scoreClosing '}' = 1197
scoreClosing '>' = 25137
scoreClosing _ = 0

-- >>> scoreCompletion "[({<"
-- 294
scoreCompletion :: [Char] -> Int
scoreCompletion = foldl' (\s c -> s * 5 + scoreFor c) 0
  where
    scoreFor :: Char -> Int
    scoreFor '(' = 1
    scoreFor '[' = 2
    scoreFor '{' = 3
    scoreFor '<' = 4
    scoreFor _ = 0

middle :: Ord a => [a] -> a
middle xs = sort xs !! (length xs `div` 2)

-- | If the line fails to parse due to wrong closing, it returns Left with the
-- first wrong closing.
--
-- If it fails to parse due to a closing bracket that was never opened, it
-- returns Nothing.
--
-- If it fails to parse due to unclosed brackets, it retuns a list of unclosed
-- brackets in the order they need to be closed. If it succeeds in parsing, the
-- list of unclosed brackets will be empty.
--
-- >>> parseLine "" "(]"
-- Just (Left ']')
--
-- >>> parseLine "" "()"
-- Just (Right "")
--
-- >>> parseLine "" "((())"
-- Just (Right "(")
--
-- >>> parseLine "" "(((])"
-- Just (Left ']')
--
-- >>> parseLine "" "]"
-- Nothing
--
-- >>> parseLine "" "[({(<(())[]>[[{[]{<()<>>"
-- Just (Right "{{[[({([")
parseLine ::
  -- | Opened Brackets
  String ->
  -- | Unparsed Brackets
  String ->
  Maybe (Either Char String)
parseLine openBrackets "" =
  Just $ Right openBrackets
parseLine "" (next : remaining)
  | isOpener next = parseLine [next] remaining
  | otherwise = Nothing
parseLine (lastOpened : restOpened) (next : remaining)
  | lastOpened `isClosedBy` next = parseLine restOpened remaining
  | isOpener next = parseLine (next : lastOpened : restOpened) remaining
  | otherwise = Just (Left next)

isOpener :: Char -> Bool
isOpener c =
  c == '('
    || c == '['
    || c == '{'
    || c == '<'

isClosedBy :: Char -> Char -> Bool
'(' `isClosedBy` ')' = True
'[' `isClosedBy` ']' = True
'{' `isClosedBy` '}' = True
'<' `isClosedBy` '>' = True
_ `isClosedBy` _ = False

readInput :: IO [String]
readInput = lines <$> getContents
