module Day01 where

day1_1 :: IO ()
day1_1 = do
  depths <- readInput
  let increases = countIncreases depths
  putStrLn $ "Increases: " <> show increases

day1_2 :: IO ()
day1_2 = do
  as <- readInput
  let bs = tail as
      cs = tail bs
      windows = zip3 as bs cs
      threeMeasurements = map (\(a,b,c) -> a + b + c) windows
      increases = countIncreases threeMeasurements
  putStrLn $ "Increases: " <> show increases

countIncreases :: [Int] -> Int
countIncreases input = length $ filter (\(a, b) -> b > a) $ zip input (tail input)

readInput :: IO [Int]
readInput =
  map read . lines <$> getContents
