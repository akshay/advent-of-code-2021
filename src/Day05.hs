{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TupleSections #-}

module Day05 (day5_1, day5_2) where

import Data.Char (isDigit)
import Data.Map (Map)
import qualified Data.Map.Lazy as Map
import Text.ParserCombinators.ReadP

day5_1 :: IO ()
day5_1 = do
  lss <- readInput
  let nonDiagonalSegments = filter (not . (==) Diagonal . slope) lss
  putStrLn $ "Number of intersection points: " <> show (countIntersections nonDiagonalSegments)

day5_2 :: IO ()
day5_2 = do
  lss <- readInput
  putStrLn $ "Number of intersection points: " <> show (countIntersections lss)

countIntersections :: [LineSegment] -> Int
countIntersections lss
  =
  let points = concatMap intPointsOnLineSeg lss
      pointOccurances = countOccurances points
      intersectionPoints = Map.filter (> 1) pointOccurances
   in Map.size intersectionPoints

-- |
-- >>> countOccurances "abcdaabb"
-- fromList [('a',3),('b',3),('c',1),('d',1)]
countOccurances :: (Eq a, Ord a) => [a] -> Map a Int
countOccurances xs = Map.fromListWith (+) $ zip xs (repeat 1)

data LineSegment = (Int, Int) :-> (Int, Int)
  deriving (Show, Eq)

x :: (a, b) -> a
x = fst

y :: (a, b) -> b
y = snd

-- |
--
-- >>> intPointsOnLineSeg $ (1, 10) :-> (1, 5)
-- [(1,10),(1,9),(1,8),(1,7),(1,6),(1,5)]
--
-- >>> intPointsOnLineSeg $ (1,1) :-> (3,3)
-- [(1,1),(2,2),(3,3)]
--
-- >>> intPointsOnLineSeg $ (1,3) :-> (3,1)
-- [(1,3),(2,2),(3,1)]
intPointsOnLineSeg :: LineSegment -> [(Int, Int)]
intPointsOnLineSeg l@(a :-> b) =
  case slope l of
    Horizontal -> (,y a) <$> enumFromToAnyDir (x a) (x b)
    Vertical -> (x a,) <$> enumFromToAnyDir (y a) (y b)
    Diagonal -> zip (enumFromToAnyDir (x a) (x b)) (enumFromToAnyDir (y a) (y b))

-- |
--
-- >>> enumFromToAnyDir 2 5
-- [2,3,4,5]
--
-- >>> enumFromToAnyDir 5 2
-- [5,4,3,2]
--
-- >>> enumFromToAnyDir 5 5
-- [5]
enumFromToAnyDir :: (Ord a, Enum a) => a -> a -> [a]
enumFromToAnyDir a b
  | a < b = [a .. b]
  | otherwise = reverse [b .. a]

slope :: LineSegment -> Slope
slope (a :-> b) =
  let xdiff = (x a - x b)
      ydiff = (y a - y b)
   in if
          | xdiff == 0 -> Vertical
          | ydiff == 0 -> Horizontal
          | otherwise -> Diagonal

data Slope = Horizontal | Vertical | Diagonal
  deriving (Eq)

readInput :: IO [LineSegment]
readInput = do
  inputStr <- getContents
  case readP_to_S (sepBy1 readLS (char '\n') <* eof) inputStr of
    [] -> error "No Parse"
    [(lss, "")] -> pure lss
    matches -> error $ "Many mathces: " <> show matches

-- >>> last $ readP_to_S readLS "0,9 -> 5,9"
-- ((0,9) :-> (5,9),"")
readLS :: ReadP LineSegment
readLS =
  (:->)
    <$> readCoords
    <* string " -> "
    <*> readCoords

readCoords :: ReadP (Int, Int)
readCoords =
  (,)
    <$> readInt
    <* char ','
    <*> readInt

readInt :: ReadP Int
readInt =
  read <$> munch1 isDigit
