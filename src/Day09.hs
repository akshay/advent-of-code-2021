module Day09 where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (mapMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.List as List

day9_1 :: IO ()
day9_1 = do
  heightmap <- readInput
  let lowestPoints = Map.filterWithKey (isSmallerThanNeighbours heightmap) heightmap
      riskLevels = Map.map (+ 1) lowestPoints
  putStrLn $ "Sum of low points: " <> show (sum riskLevels)

day9_2 :: IO ()
day9_2 = do
  heightmap <- readInput
  let lowestPoints = Map.filterWithKey (isSmallerThanNeighbours heightmap) heightmap
      basins = map (\p -> fillBasin heightmap p mempty) $ Map.keys lowestPoints
      sortedBasins = List.sortOn (negate . Set.size) basins
  putStrLn $ "Product of biggest basins: " <> show (product . map Set.size $ take 3 sortedBasins)

lookupMany :: [(Int, Int)] -> HeightMap a -> [a]
lookupMany ps m = mapMaybe (m Map.!?) ps

neighbourPositions :: (Int, Int) -> [(Int, Int)]
neighbourPositions (i, j) =
  [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)]

isSmallerThanNeighbours :: Ord a => HeightMap a -> (Int, Int) -> a -> Bool
isSmallerThanNeighbours m pos x =
  x < minimum (lookupMany (neighbourPositions pos) m)

fillBasin :: HeightMap Int -> (Int, Int) -> Set (Int, Int) ->  Set (Int, Int)
fillBasin m pos includedPoints  =
  let ns = Set.fromList $ neighbourPositions pos
      newPoints = Set.difference ns includedPoints
      non9Points = Map.keysSet . Map.filter (/= 9) $ Map.restrictKeys m newPoints
   in foldr (fillBasin m) (Set.union includedPoints non9Points) non9Points

type HeightMap a = Map (Int, Int) a

readInput :: IO (HeightMap Int)
readInput = do
  ls <- lines <$> getContents
  pure . Map.fromList . concat $
    zipWith
      ( \l i ->
          zipWith
            (\c j -> ((i, j), read [c]))
            l
            [0 ..]
      )
      ls
      [0 ..]
