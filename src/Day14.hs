{-# LANGUAGE TupleSections #-}

module Day14 where

import qualified Data.Char as Char
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Text.ParserCombinators.ReadP

day14_1 :: IO ()
day14_1 = do
  (template, rules) <- readInput
  let after10Steps = foldr (const $ applyRules rules) template [(1 :: Int) .. 10]
      f = freqs after10Steps
      mostCommon = maximum f
      leastCommon = minimum f
  putStrLn $ "After 10 steps: Most common - least common: " <> show (mostCommon - leastCommon)

applyRules :: Rules -> Template -> Template
applyRules rules t =
  let pairs = zip t (tail t)
      insertions = map (rules Map.!) pairs
   in (head t :) . List.intercalate "" $
        zipWith (\c i -> i : [c]) (tail t) insertions

freqs :: Ord a => [a] -> Map a Integer
freqs = foldr (\x -> Map.insertWith (+) x 1) mempty

day14_2 :: IO ()
day14_2 = do
  (template, rules) <- readInput
  let buckets = mkBuckets template
      after40Steps = foldr (const $ applyRulesToBuckets rules) buckets [(1 :: Int) .. 40]
      f = freqsFromBuckets (head template) after40Steps
      mostCommon = maximum f
      leastCommon = minimum f
  putStrLn $ "After 40 steps: Most common - least common: " <> show (mostCommon - leastCommon)

type Buckets = Map (Char, Char) Integer

mkBuckets :: Template -> Buckets
mkBuckets t =
  Map.fromList $ (,1) <$> zip t (tail t)

applyRulesToBuckets :: Rules -> Buckets -> Buckets
applyRulesToBuckets rules =
  Map.foldrWithKey
    ( \(p1, p2) n ->
        let p3 = rules Map.! (p1, p2)
         in Map.unionWith (+) (Map.fromList [((p1, p3), n), ((p3, p2), n)])
    )
    mempty

-- >>> freqsFromBuckets 'N' (Map.fromList [(('N', 'C'), 1)]) -- NC
-- fromList [('C',1),('N',1)]
--
-- >>> freqsFromBuckets 'N' (Map.fromList [(('N', 'C'), 1), (('C', 'B'), 1)]) -- NCB
-- fromList [('B',1),('C',1),('N',1)]
--
-- >>> freqsFromBuckets 'N' (Map.fromList [(('N', 'C'), 1), (('C', 'B'), 2), (('B', 'C'), 1)]) -- NCBCB
-- fromList [('B',2),('C',2),('N',1)]
--
-- >>> freqsFromBuckets 'N' (Map.fromList [(('N', 'N'), 1), (('N', 'C'), 1), (('C', 'B'), 2), (('B', 'C'), 1)]) -- NNCBCB
-- fromList [('B',2),('C',2),('N',2)]
freqsFromBuckets :: Char -> Buckets -> Map Char Integer
freqsFromBuckets firstChar b =
  -- Every character apart from the first and the last one is duplicated when
  -- seen as pairs, so we only count the frequency of the second character in a
  -- pair, this would get us the frequence of all characters except for the
  -- first one. So then we can add 1 to the frequency of the first character.
  let freqsWithoutFirstChar = Map.foldrWithKey (\(_, p2) n -> Map.unionWith (+) (Map.fromList [(p2, n)])) mempty b
   in Map.insertWith (+) firstChar 1 freqsWithoutFirstChar

type Template = String

type Rules = Map (Char, Char) Char

readInput :: IO (Template, Rules)
readInput = do
  inputStr <- getContents
  let parser = ((,) <$> readTemplate <* string "\n\n" <*> readRules) <* eof
  case readP_to_S parser inputStr of
    [] -> error "No Parse"
    [(i, "")] -> pure i
    xs -> error $ "Many parse: " <> show xs

readTemplate :: ReadP Template
readTemplate = munch1 Char.isUpper

readRules :: ReadP Rules
readRules = Map.fromList <$> sepBy1 readRule (char '\n')

readRule :: ReadP ((Char, Char), Char)
readRule = do
  c1 <- satisfy Char.isUpper
  c2 <- satisfy Char.isUpper
  _ <- string " -> "
  c3 <- satisfy Char.isUpper
  pure ((c1, c2), c3)
