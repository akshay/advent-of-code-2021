module Day06 where

import Data.List.Extra (splitOn)
import Data.Map (Map)
import qualified Data.Map.Lazy as Map

day6_1 :: IO ()
day6_1 = do
  initialFish <- readInput
  let finalFish = foldrN tick initialFish 80
  putStrLn $ "Final Fish: " <> show (length finalFish)

day6_2 :: IO ()
day6_2 = do
  initialFish <- readInput
  -- 256 = 7 * 36 + 4. So we tick all the fish 4 times. After that we treat them
  -- as a school and use 'tick7School' to tick them 36 times.
  let after4Fish = foldrN tick initialFish 4
      after4School = mkSchool after4Fish
      finalSchool = foldrN tick7School after4School 36
      finalFishCount = sum $ Map.elems finalSchool
  putStrLn $ "Final Fish: " <> show finalFishCount

foldrN :: (a -> a) -> a -> Int -> a
foldrN f z n = foldr (const f) z (replicate n ())

-- | Inefficient, but works for the first case.
--
-- >>> tick [6]
-- [5]
--
-- >>> tick [0]
-- [6,8]
--
-- >>> length $ foldrN tick [3,4,3,1,2] 18
-- 26
tick :: [Int] -> [Int]
tick = concatMap (\f -> if f == 0 then [6, 8] else [f - 1])

type School = Map Int Int

mkSchool :: [Int] -> School
mkSchool fs = Map.fromListWith (+) $ zip fs (repeat 1)

-- | Ticks a school 7 times
--
-- >>> tick7School $ Map.singleton 6 1
-- fromList [(0,0),(1,0),(2,0),(3,0),(4,0),(5,0),(6,1),(7,0),(8,1)]
--
-- >>> tick7School $ Map.singleton 6 0
-- fromList [(0,0),(1,0),(2,0),(3,0),(4,0),(5,0),(6,0),(7,0),(8,0)]
tick7School :: School -> School
tick7School school =
  foldr
    ( \f newSchool ->
        let newFish = tick7Fish f
            parents = Map.findWithDefault 0 f school
         in foldr (\nf -> Map.insertWith (+) nf parents) newSchool newFish
    )
    mempty
    [0 .. 8]

-- >>> tick7Fish 6 == foldrN tick [6] 7
-- True
tick7Fish :: Int -> [Int]
tick7Fish f = if f <= 6 then [f, f + 2] else [f - 7]

readInput :: IO [Int]
readInput =
  map read . splitOn "," <$> getContents
