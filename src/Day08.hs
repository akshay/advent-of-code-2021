module Day08 where

import Data.Char
import qualified Data.List as List
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Text.ParserCombinators.ReadP

day8_1 :: IO ()
day8_1 = do
  outputs <- concatMap snd <$> readInput
  let easyTargetCount = length $ filter matchEasyTargets outputs
  putStrLn $ "Easy targets: " <> show easyTargetCount

day8_2 :: IO ()
day8_2 = do
  entries <- readInput
  let outputs = map solveEntry entries
  putStrLn $ "Sum of outputs: " <> show (sum outputs)

solveEntry :: ([Pattern], [Pattern]) -> Int
solveEntry (inputDigits, outputDigits) =
  let (one, notOnes) = matchOneAndPartition isOne inputDigits
      (four, notFours) = matchOneAndPartition isFour notOnes
      (seven, notSevens) = matchOneAndPartition isSeven notFours
      (eight, notEights) = matchOneAndPartition isEight notSevens
      (nine, notNines) = matchOneAndPartition (isNine four seven) notEights
      (two, notTwos) = matchOneAndPartition (isTwo four seven) notNines
      (three, notThrees) = matchOneAndPartition (isThree one) notTwos
      (five, notFives) = matchOneAndPartition isFive notThrees
      (six, [zero]) = matchOneAndPartition (isSix five) notFives
      lookupTable = Map.fromList [(zero, 0), (one, 1), (two, 2), (three, 3), (four, 4), (five, 5), (six, 6), (seven, 7), (eight, 8), (nine, 9)]
   in toInt $ map (lookupTable Map.!) outputDigits

matchOneAndPartition :: Show a => (a -> Bool) -> [a] -> (a, [a])
matchOneAndPartition p xs =
  let (p1, p2) = List.partition p xs
   in case p1 of
        [m] -> (m, p2)
        _ -> error $ "Expected exactly one match, got: " <> show (p1, p2)

-- >>> toInt [1,2,3,4]
-- 1234
toInt :: [Int] -> Int
toInt = snd . foldr (\d (m, acc) -> (m * 10, acc + d * m)) (1, 0)

isOne :: Pattern -> Bool
isOne = (== 2) . Set.size

isFour :: Pattern -> Bool
isFour = (== 4) . Set.size

isSeven :: Pattern -> Bool
isSeven = (== 3) . Set.size

isEight :: Pattern -> Bool
isEight = (== 7) . Set.size

isTwo :: Pattern -> Pattern -> Pattern -> Bool
isTwo four seven unknown =
  let fourPlusSeven = Set.union four seven
      fsm = Set.size $ fourPlusSeven `Set.difference` unknown -- (7 + 4) - unknown
      mfs = Set.size $ unknown `Set.difference` fourPlusSeven -- unknown - (7 + 4)
   in fsm == 2 && mfs == 2

isNine :: Pattern -> Pattern -> Pattern -> Bool
isNine four seven unknown =
  let fourPlusSeven = Set.union four seven
      fsm = Set.size $ fourPlusSeven `Set.difference` unknown -- (7 + 4) - unknown
      mfs = Set.size $ unknown `Set.difference` fourPlusSeven -- unknown - (7 + 4)
   in fsm == 0 && mfs == 1

-- Can only be ascertained after 2 and 3 are identified
isFive :: Pattern -> Bool
isFive = (== 5) . length

-- Can only be ascertained after 0 and 9 are identified
isSix :: Pattern -> Pattern -> Bool
isSix five unknown =
  Set.size (unknown `Set.difference` five) == 1

isThree :: Pattern -> Pattern -> Bool
isThree one unknown =
  Set.size (unknown `Set.difference` one) == 3

matchEasyTargets :: Pattern -> Bool
matchEasyTargets p =
  or $ [isOne, isFour, isSeven, isEight] <*> [p]

type Pattern = Set Char

readInput :: IO [([Pattern], [Pattern])]
readInput = do
  inputStr <- getContents
  case readP_to_S (sepBy1 readLine (char '\n') <* eof) inputStr of
    [] -> error "No match"
    [(input, "")] -> pure input
    xs -> error $ "Many matches: " <> show xs

-- >>> readP_to_S (readLine <* eof) "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
-- [((["acedgfb","cdfbe","gcdfa","fbcad","dab","cefabd","cdfgeb","eafb","cagedb","ab"],["cdfeb","fcadb","cdfeb","cdbaf"]),"")]
readLine :: ReadP ([Pattern], [Pattern])
readLine = do
  leftPattern <- sepBy1 readPattern (char ' ')
  _ <- string " | "
  rightPattern <- sepBy1 readPattern (char ' ')
  pure (leftPattern, rightPattern)

readPattern :: ReadP Pattern
readPattern =
  Set.fromList <$> munch1 (not . isSpace)
