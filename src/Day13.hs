module Day13 where

import Control.Applicative ((<|>))
import qualified Data.Char as Char
import qualified Data.List as List
import Data.Set (Set)
import qualified Data.Set as Set
import Text.ParserCombinators.ReadP

day13_1 :: IO ()
day13_1 = do
  (cs, is) <- readInput
  i <- case is of
    [] -> error "No instructions!"
    (i : _) -> pure i
  let after1Instruction = foldPaper cs i
  putStrLn $ "Visible Dots: " <> show (Set.size after1Instruction)

day13_2 :: IO ()
day13_2 = do
  (cs, is) <- readInput
  let folded = List.foldl' foldPaper cs is
  putStrLn $ renderCoords folded

foldPaper :: Set Coords -> Instruction -> Set Coords
foldPaper cs (FoldAlongX axis) = foldAlongX cs axis
foldPaper cs (FoldAlongY axis) = foldAlongY cs axis

-- >>> foldAlongY (Set.fromList [(6,6)]) 5
-- fromList [(6,4)]
--
-- >>> foldAlongY (Set.fromList [(6,6)]) 7
-- fromList [(6,6)]
--
-- >>> foldAlongY (Set.fromList [(6,6), (6,4)]) 5
-- fromList [(6,4)]
--
-- >>> foldAlongY (Set.fromList [(6,6), (6,3)]) 5
-- fromList [(6,3),(6,4)]
foldAlongY :: Set Coords -> Int -> Set Coords
foldAlongY = foldGeneric (\foldAxis (_, y) -> y < foldAxis) mirrorY

-- >>> foldAlongX (Set.fromList [(6,6)]) 5
-- fromList [(4,6)]
--
-- >>> foldAlongX (Set.fromList [(6,6)]) 7
-- fromList [(6,6)]
--
-- >>> foldAlongX (Set.fromList [(6,6), (4,6)]) 5
-- fromList [(4,6)]
--
-- >>> foldAlongX (Set.fromList [(6,6), (3,6)]) 5
-- fromList [(3,6),(4,6)]
foldAlongX :: Set Coords -> Int -> Set Coords
foldAlongX = foldGeneric (\foldAxis (x, _) -> x < foldAxis) mirrorX

foldGeneric ::
  -- | Function to partition the coordinates into those which are to be included
  -- as is and those which need to be mirrored. Should return 'True' for the
  -- coordinates which are to be included as is.
  (Int -> Coords -> Bool) ->
  -- | Function to mirror the coordinates.
  (Int -> Coords -> Coords) ->
  -- | Input set
  Set Coords ->
  -- | Axis of the fold
  Int ->
  Set Coords
foldGeneric partitionF mirrorF cs foldAxis =
  let (coordsIncluded, coordsFolded) = Set.partition (partitionF foldAxis) cs
      coordsFoldedMirrored = Set.map (mirrorF foldAxis) coordsFolded
   in Set.union coordsIncluded coordsFoldedMirrored

-- >>> mirrorY 5 (6,6)
-- (6,4)
mirrorY :: Int -> Coords -> Coords
mirrorY mirrorAxis (x, y) = (x, (2 * mirrorAxis) - y)

-- >>> mirrorX 5 (6,6)
-- (4,6)
mirrorX :: Int -> Coords -> Coords
mirrorX mirrorAxis (x, y) = ((2 * mirrorAxis) - x, y)

renderCoords :: Set Coords -> String
renderCoords cs =
  let maxX = maximum $ Set.map fst cs
      maxY = maximum $ Set.map snd cs
      line y = map (\x -> if (x, y) `elem` cs then '#' else '.') [0 .. maxX]
   in unlines $ map line [0 .. maxY]

type Coords = (Int, Int)

data Instruction = FoldAlongX Int | FoldAlongY Int
  deriving (Show, Eq)

readInput :: IO (Set Coords, [Instruction])
readInput = do
  inputStr <- getContents
  case readP_to_S (readAll <* eof) inputStr of
    [] -> error "No parse"
    [(i, "")] -> pure i
    xs -> error $ "Many parse: " <> show xs

readAll :: ReadP (Set Coords, [Instruction])
readAll = do
  cs <- Set.fromList <$> sepBy readCoords (char '\n')
  _ <- string "\n\n"
  is <- sepBy readInstruction (char '\n')
  pure (cs, is)

readInstruction :: ReadP Instruction
readInstruction = do
  _ <- string "fold along "
  constr <- (FoldAlongX <$ char 'x') <|> (FoldAlongY <$ char 'y')
  _ <- char '='
  constr <$> readInt

readCoords :: ReadP Coords
readCoords =
  (,)
    <$> readInt
    <* char ','
    <*> readInt

readInt :: ReadP Int
readInt =
  read <$> munch1 Char.isDigit
