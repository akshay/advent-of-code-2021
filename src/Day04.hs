{-# LANGUAGE TupleSections #-}

module Day04 where

import Control.Monad (mfilter)
import Data.List (intercalate)
import Data.List.Extra (splitOn)
import Data.Matrix (Matrix)
import qualified Data.Matrix as Matrix
import Text.ParserCombinators.ReadP (ReadP, char, eof, munch1, readP_to_S, sepBy1)
import Data.Char (isDigit)
import Control.Applicative ((<|>))

day4_1 :: IO ()
day4_1 = do
  (numbers, boards) <- readInput
  case callNumbers numbers boards of
    Nothing -> putStrLn "No winners"
    Just (n, b) -> do
      putStrLn $ "Winner:\n" <> showMatrix b <> "\n"
      putStrLn $ "Winner Score: " <> show (calcScore n b)

day4_2 :: IO ()
day4_2 = do
  (numbers, boards) <- readInput
  case callNumbersUntilOneBoardRemains numbers boards of
    Nothing -> putStrLn "That never happened!"
    Just (ns, b) -> do
      case callNumbers ns [b] of
        Nothing -> putStrLn $ "This board never wins: \n" <> showMatrix b <> "\nRemaining Numbers: " <> show ns
        Just (n, finallyWonBoard) -> do
          putStrLn $ "Loser: \n" <> showMatrix finallyWonBoard
          putStrLn $ "Loser Score: " <> show (calcScore n finallyWonBoard)

calcScore :: Int -> Board -> Int
calcScore n b =
  let unmarkedNums = fmap fst . mfilter (not . snd) $ Matrix.flatten b
   in sum unmarkedNums * n

type Board = Matrix (Int, Bool)

markBoard :: Int -> Board -> Board
markBoard calledNumber = Matrix.map (\(n, previousMarking) -> (n, previousMarking || n == calledNumber))

markBoards :: Int -> [Board] -> [Board]
markBoards calledNumber = map (markBoard calledNumber)

hasWon :: Board -> Bool
hasWon b =
  any (all snd) (Matrix.toRows b)
    || any (all snd) (Matrix.toColumns b)

callNumbers :: [Int] -> [Board] -> Maybe (Int, Board)
callNumbers [] _ = Nothing
callNumbers (n : ns) bs =
  let markedBs = markBoards n bs
      wonBoards = filter hasWon markedBs
   in case wonBoards of
        [] -> callNumbers ns markedBs
        winner : _ -> Just (n, winner)

callNumbersUntilOneBoardRemains :: [Int] -> [Board] -> Maybe ([Int], Board)
callNumbersUntilOneBoardRemains [] _ = Nothing
callNumbersUntilOneBoardRemains ns [b] = Just (ns, b)
callNumbersUntilOneBoardRemains (n : ns) bs =
  let markedBs = markBoards n bs
      notWonBoards = filter (not . hasWon) markedBs
   in callNumbersUntilOneBoardRemains ns notWonBoards

showMatrix :: Board -> String
showMatrix =
  intercalate "\n" . map show . Matrix.toLists

readInput :: IO ([Int], [Board])
readInput = do
  calledNumbersStr <- getLine
  let calledNumbers = map read $ splitOn "," calledNumbersStr

  _ <- getLine
  boardsStr <- getContents
  bs <- case readP_to_S readBoards boardsStr of
    [] -> error "No Parse"
    [(boards, "")] -> pure boards
    bs -> error $ "Many matches: " <> show bs
  pure (calledNumbers, bs)

readBoards :: ReadP [Board]
readBoards =
  sepBy1 readBoard (char '\n' >> char '\n') <* eof

readBoard :: ReadP Board
readBoard =
  Matrix.fromLists <$> sepBy1 readRow (char '\n')

-- >>> last $ readP_to_S readRow " 8  2 23  4 24"
-- ([(8,False),(2,False),(23,False),(4,False),(24,False)],"")
readRow :: ReadP [(Int, Bool)]
readRow = do
  map (,False) <$> sepBy1 readNum (char ' ')

-- >>> readP_to_S readNum " 8"
-- [(8,"")]
--
-- >>> readP_to_S readNum "23"
-- [(23,"")]
readNum :: ReadP Int
readNum = do
  _ <- (char ' ' >> pure ()) <|> pure ()
  read <$> munch1 isDigit
