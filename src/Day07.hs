module Day07 where

import Data.List.Extra (splitOn)

day7_1 :: IO ()
day7_1 = do
  crabs <- readInput
  let minFuel = minimum $ map (alignmentCost crabs) [minimum crabs .. maximum crabs]
  putStrLn $ "Min Fuel: " <> show minFuel

day7_2 :: IO ()
day7_2 = do
  crabs <- readInput
  let minFuel = minimum $ map (actualAlignmentCost crabs) [minimum crabs .. maximum crabs]
  putStrLn $ "Min Fuel: " <> show minFuel

-- >>> alignmentCost [16,1,2,0,4,2,7,1,2,14] 2
-- 37
alignmentCost :: [Int] -> Int -> Int
alignmentCost crabs n =
  sum $ map (abs . (n -)) crabs

-- >>> actualAlignmentCost [16,1,2,0,4,2,7,1,2,14] 2
-- 206
actualAlignmentCost :: [Int] -> Int -> Int
actualAlignmentCost crabs n =
  sum $
    map
      ( \c ->
          let steps = abs (c - n)
           in (steps * (steps + 1)) `div` 2
      )
      crabs

readInput :: IO [Int]
readInput =
  map read . splitOn "," <$> getContents
