{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}

module Day16 where

import Control.Monad
import Data.Attoparsec.Combinator
import qualified Data.Attoparsec.Internal.Types as A
import Data.Bits (setBit, testBit)
import qualified Data.Bits
import qualified Data.Char as Char
import Data.List (intercalate)

day16_1 :: IO ()
day16_1 = do
  bits <- readInput
  case parse packetParser bits of
    A.Done _ p -> putStrLn $ "Version Sum: " <> show (versionSum p)
    f -> putStrLn $ "Failed to parse: " <> show f

day16_2 :: IO ()
day16_2 = do
  bits <- readInput
  case parse packetParser bits of
    A.Done _ p -> do
      putStrLn $ "Expr: " <> showPacket p
      putStrLn $ "Eval: " <> show (eval p)
    f -> putStrLn $ "Failed to parse: " <> show f

data Packet = Packet {version :: Int, dat :: Expr}
  deriving (Show, Eq)

data Expr
  = Literal Int
  | Sum [Packet]
  | Product [Packet]
  | Minimum [Packet]
  | Maximum [Packet]
  | GreaterThan Packet Packet
  | LessThan Packet Packet
  | EqualTo Packet Packet
  deriving (Show, Eq)

versionSum :: Packet -> Int
versionSum p =
  let f = sum . map versionSum
   in version p + case dat p of
        Literal _ -> 0
        Sum ps -> f ps
        Product ps -> f ps
        Minimum ps -> f ps
        Maximum ps -> f ps
        GreaterThan p1 p2 -> f [p1, p2]
        LessThan p1 p2 -> f [p1, p2]
        EqualTo p1 p2 -> f [p1, p2]

eval :: Packet -> Int
eval (Packet _ (Literal n)) = n
eval (Packet _ (Sum ps)) = sum $ map eval ps
eval (Packet _ (Product ps)) = product $ map eval ps
eval (Packet _ (Minimum ps)) = minimum $ map eval ps
eval (Packet _ (Maximum ps)) = maximum $ map eval ps
eval (Packet _ (GreaterThan p1 p2)) = boolToInt $ eval p1 > eval p2
eval (Packet _ (LessThan p1 p2)) = boolToInt $ eval p1 < eval p2
eval (Packet _ (EqualTo p1 p2)) = boolToInt $ eval p1 == eval p2

showPacket :: Packet -> String
showPacket (Packet _ (Literal n)) = show n
showPacket (Packet _ (Sum ps)) = "sum(" <> intercalate "," (map showPacket ps) <> ")"
showPacket (Packet _ (Product ps)) = "product(" <> intercalate "," (map showPacket ps) <> ")"
showPacket (Packet _ (Minimum ps)) = "minimum(" <> intercalate "," (map showPacket ps) <> ")"
showPacket (Packet _ (Maximum ps)) = "maximum(" <> intercalate "," (map showPacket ps) <> ")"
showPacket (Packet _ (GreaterThan p1 p2)) = "(" <> showPacket p1 <> " > " <> showPacket p2 <> ")"
showPacket (Packet _ (LessThan p1 p2)) = "(" <> showPacket p1 <> " < " <> showPacket p2 <> ")"
showPacket (Packet _ (EqualTo p1 p2)) = "(" <> showPacket p1 <> " = " <> showPacket p2 <> ")"

boolToInt :: Bool -> Int
boolToInt True = 1
boolToInt False = 0

type Bit = Bool

newtype Bits = Bits [Bit]
  deriving newtype (Semigroup, Monoid, Show)

type Parser = A.Parser Bits

-- >>> parse packetParser . Bits $ concatMap mkBits "D2FE28"
-- Done [] (Packet {version = 6, dat = Literal 2021})
--
-- >>> fmap showPacket . parse packetParser . Bits $ concatMap mkBits "38006F45291200"
-- Done [] "(10 < 20)"
packetParser :: Parser Packet
packetParser = Packet <$> versionParser <*> contentParser

-- >>> parse versionParser . Bits $ concatMap mkBits "D2FE28"
-- Done [] 6
versionParser :: Parser Int
versionParser = intParser 3

typeIdParser :: Parser Int
typeIdParser = intParser 3

contentParser :: Parser Expr
contentParser = do
  typeId <- typeIdParser
  case typeId of
    0 -> Sum <$> operatorParser
    1 -> Product <$> operatorParser
    2 -> Minimum <$> operatorParser
    3 -> Maximum <$> operatorParser
    4 -> Literal <$> literalParser
    5 -> uncurry GreaterThan <$> assertTwo operatorParser
    6 -> uncurry LessThan <$> assertTwo operatorParser
    7 -> uncurry EqualTo <$> assertTwo operatorParser
    _ -> fail $ "Invalid type id: " <> show typeId

assertTwo :: Show a => Parser [a] -> Parser (a, a)
assertTwo p = do
  xs <- p
  case xs of
    [a, b] -> pure (a, b)
    _ -> fail $ "Expected exactly two, got: " <> show xs

operatorParser :: Parser [Packet]
operatorParser = do
  lengthTypeId <- anyBit
  if lengthTypeId
    then fixedNumberOperatorParser
    else fixedLengthOperatorParser

fixedLengthOperatorParser :: Parser [Packet]
fixedLengthOperatorParser = do
  l <- intParser 15
  bs <- replicateM l anyBit
  case parse (many1 packetParser) (Bits bs) of
    A.Done _ ps -> pure ps
    xs -> fail $ "Failed to parse fixed length operator packets: " <> show xs

fixedNumberOperatorParser :: Parser [Packet]
fixedNumberOperatorParser = do
  n <- intParser 11
  replicateM n packetParser

-- >>> parse literalParser . Bits $ fromString "1000100100"
-- Done [] 20
literalParser :: Parser Int
literalParser = do
  nonLastGroups <- concat <$> many' nonLastLiteralGroupParser
  lastGroup <- lastLiteralGroupParser
  pure . mkInt $ nonLastGroups <> lastGroup

-- >>> fmap mkInt . parse literalParser' $ fromString "1000100100"
-- Done [] 20
literalParser' :: Parser [Bit]
literalParser' = do
  nonLastGroups <- concat <$> many' nonLastLiteralGroupParser
  lastGroup <- lastLiteralGroupParser
  pure $ nonLastGroups <> lastGroup

fromString :: String -> [Bit]
fromString = map ((== 1) . Char.digitToInt)

-- >>> mkInt <$> parse (nonLastLiteralGroupParser) (Bits $ fromString "10010")
-- Done [] 2
--
-- >>> parse nonLastLiteralGroupParser (Bits $ fromString "00000")
-- Fail [False,False,False,False,False] [] "Failed reading: Found last literal group"
--
-- >>> parse nonLastLiteralGroupParser (Bits $ fromString "000")
-- Fail [False,False,False] [] "Failed reading: Found last literal group"
nonLastLiteralGroupParser :: Parser [Bool]
nonLastLiteralGroupParser = do
  continue <- anyBit
  if continue
    then replicateM 4 anyBit
    else fail "Found last literal group"

lastLiteralGroupParser :: Parser [Bool]
lastLiteralGroupParser = do
  continue <- anyBit
  if continue
    then fail "Found non last literal group"
    else replicateM 4 anyBit

intParser :: Int -> Parser Int
intParser n =
  mkInt <$> replicateM n anyBit

-- >>> mkInt [True, True, False]
-- 6
mkInt :: [Bit] -> Int
mkInt = foldr (\(n, b) acc -> if b then setBit acc n else acc) 0 . zip [(0 :: Int) ..] . reverse

anyBit :: Parser Bit
anyBit = satisfy (const True)

satisfy :: (Bit -> Bool) -> Parser Bit
satisfy = satisfyElem

parse :: Parser a -> Bits -> A.IResult Bits a
parse p (Bits bs) = A.runParser p bs (A.Pos 0) A.Complete (\remainingBs _ _ ss s -> A.Fail (Bits remainingBs) ss s) (\_ _ _ a -> A.Done mempty a)

type instance A.State Bits = [Bit]

instance A.Chunk Bits where
  type ChunkElem Bits = Bit
  nullChunk (Bits bs) = null bs
  pappendChunk state (Bits bs) = state <> bs
  atBufferEnd _ = A.Pos . subtract 1 . length
  bufferElemAt _ (A.Pos p) xs
    | p < length xs = Just (xs !! p, 1)
    | otherwise = Nothing
  chunkElemToChar _ b = if b then '1' else '0'

readInput :: IO Bits
readInput =
  Bits . concatMap mkBits <$> getLine

-- >>> mkBits '9'
-- [True,False,False,True]
mkBits :: Char -> [Bit]
mkBits = extractBits [3,2,1,0] . Char.digitToInt

extractBits :: Data.Bits.Bits a => [Int] -> a -> [Bit]
extractBits indices n = map (testBit n) indices
