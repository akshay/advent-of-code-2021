module Day12 where

import Control.Applicative ((<|>))
import qualified Data.Char as Char
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Debug.Trace as Debug
import Text.ParserCombinators.ReadP

day12_1 :: IO ()
day12_1 = do
  g <- readInput
  let ps = paths g Start
  putStrLn $ "Number of Paths: " <> show (length ps)

day12_2 :: IO ()
day12_2 = do
  g <- readInput
  let ps = pathsV2 g (Once []) Start
  putStrLn $ "Number of Paths: " <> show (length ps)

paths :: Graph -> Cave -> [Path]
paths _ End = [[End]]
paths g c =
  let neighbours = Map.findWithDefault [] c g
      g' =
        -- If the cave is small, delete all edges leading from the cave before
        -- exploring more. Destroying exists of a cave is enough to stop it from
        -- being visited again.
        if isSmall c
          then Map.delete c g
          else g
   in map (c :) $ concatMap (paths g') neighbours

isSmall :: Cave -> Bool
isSmall (BigCave _) = False
isSmall _ = True

pathsV2 :: Graph -> SmallCaveVisits -> Cave -> [Path]
pathsV2 _ _ End = [[End]]
pathsV2 g visits c =
  let neighbours = Map.findWithDefault [] c g
      (visits', destroyCaves) = updateVisits visits c
      g' = Map.filterWithKey (\k _ -> k `notElem` destroyCaves) g
   in map (c :) $ concatMap (pathsV2 g' visits') neighbours

data Cave = Start | BigCave String | SmallCave String | End
  deriving (Eq, Ord)

instance Show Cave where
  show Start = "start"
  show End = "end"
  show (BigCave x) = x
  show (SmallCave x) = x

type Edge = (Cave, Cave)

type Graph = Map Cave [Cave]

type Path = [Cave]

data SmallCaveVisits = Once [Cave] | Twice
  deriving (Show, Eq)

-- | Also returns all the caves which must be destroyed
--
-- >>> updateVisits Twice (SmallCave "a")
-- (Twice,[a])
--
-- >>> updateVisits Twice (BigCave "A")
-- (Twice,[])
--
-- >>> updateVisits (Once [SmallCave "a", SmallCave "b", SmallCave "c"]) (SmallCave "a")
-- (Twice,[a,b,c])
--
-- >>> updateVisits (Once []) Start
-- (Once [start],[start])
--
-- >>> updateVisits (Once [SmallCave "a"]) (SmallCave "b")
-- (Once [b,a],[])
--
-- >>> updateVisits (Once [SmallCave "a"]) (BigCave "B")
-- (Once [a],[])
updateVisits :: SmallCaveVisits -> Cave -> (SmallCaveVisits, [Cave])
updateVisits Twice c = (Twice, [c | isSmall c])
updateVisits (Once cs) c
  | isSmall c =
    if c `elem` cs
      then (Twice, cs)
      else (Once (c : cs), [c | c == Start])
  | otherwise = (Once cs, [])

readInput :: IO Graph
readInput = do
  inputStr <- getContents
  case readP_to_S (readGraph <* eof) inputStr of
    [] -> error "No parse"
    [(g, "")] -> pure g
    manyParse -> error $ "Many parses: " <> show manyParse

readGraph :: ReadP Graph
readGraph = do
  edges <- sepBy readEdge (char '\n')
  pure $ foldr (\(a, b) g -> Map.insertWith (<>) b [a] $ Map.insertWith (<>) a [b] g) mempty edges

readEdge :: ReadP Edge
readEdge = do
  a <- readCave
  _ <- char '-'
  b <- readCave
  pure (a, b)

readCave :: ReadP Cave
readCave = readBig <|> readSmall
  where
    readBig = BigCave <$> munch1 Char.isUpper
    readSmall = do
      name <- munch1 Char.isLower
      case name of
        "start" -> pure Start
        "end" -> pure End
        _ -> pure $ SmallCave name
