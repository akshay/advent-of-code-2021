{-# LANGUAGE LambdaCase #-}

module Day02 where

import Data.Either

day2_1 :: IO ()
day2_1 = do
  commands <- readInput
  let (finalX, finalY) = foldl executeCommand (0, 0) commands
  putStrLn $ "Solution: " <> show (finalX * finalY)
  where
    executeCommand :: (Int, Int) -> Command -> (Int, Int)
    executeCommand (x, y) = \case
      Forward f -> (x + f, y)
      Down d -> (x, y + d)
      Up u -> (x, y - u)

day2_2 :: IO ()
day2_2 = do
  commands <- readInput
  let (finalX, finalY, _finalAim) = foldl executeCommand (0, 0, 0) commands
  putStrLn $ "Solution: " <> show (finalX * finalY)
  where
    executeCommand :: (Int, Int, Int) -> Command -> (Int, Int, Int)
    executeCommand (x, y, aim) = \case
      Forward f -> (x + f, y + (aim * f), aim)
      Down d -> (x, y, aim + d)
      Up u -> (x, y, aim - u)

data Command
  = Forward Int
  | Down Int
  | Up Int

parseCommand :: String -> Either String Command
parseCommand input =
  case words input of
    [direction, magnitudeStr] ->
      let magnitude = read magnitudeStr
       in case direction of
            "forward" -> Right $ Forward magnitude
            "down" -> Right $ Down magnitude
            "up" -> Right $ Up magnitude
            _ -> Left $ "Invalid direction: " <> show input
    _ -> Left $ "Invalid command: " <> show input

readInput :: IO [Command]
readInput = do
  inputLines <- lines <$> getContents
  pure . rights $ map parseCommand inputLines
