{-# LANGUAGE TupleSections #-}

module Day11 where

import Data.Function ((&))
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.Set as Set

day11_1 :: IO ()
day11_1 = do
  os <- readInput
  let (_, flashes) = foldr (const sumFlashes) (os, 0) [(1 :: Int) .. 100]
  putStrLn $ "Flashes: " <> show flashes

day11_2 :: IO ()
day11_2 = do
  os <- readInput
  let steps = stepUntilAllFlash 0 os
  putStrLn $ "Steps: " <> show steps

renderOctopi :: OctopusMap Int -> String
renderOctopi m =
  let maxI = maximum . Set.map fst $ Map.keysSet m
      maxJ = maximum . Set.map snd $ Map.keysSet m
      line i = unwords [show (m Map.! (i, j)) | j <- [0 .. maxJ]]
   in unlines [line j | j <- [0 .. maxI]]

sumFlashes :: (OctopusMap Int, Int) -> (OctopusMap Int, Int)
sumFlashes (octopi, flashes) =
  let (newOctopi, newFlashes) = step octopi
  in (newOctopi, flashes + newFlashes)

step :: OctopusMap Int -> (OctopusMap Int, Int)
step m =
  increaseEnergy m
    & fmap (,False)
    & runFlashCycle
    & countFlashed

stepUntilAllFlash :: Int -> OctopusMap Int -> Int
stepUntilAllFlash n o =
  let (o', flashes) = step o
  in if flashes == Map.size o
     then n + 1
     else stepUntilAllFlash (n + 1) o'

increaseEnergy :: OctopusMap Int -> OctopusMap Int
increaseEnergy = fmap (+ 1)

runFlashCycle :: OctopusMap (Int, Bool) -> OctopusMap (Int, Bool)
runFlashCycle os =
  let nineAndNotFlashed = Map.keysSet $ Map.filter (\(energy, flashed) -> energy > 9 && not flashed) os
   in if Set.null nineAndNotFlashed
        then os
        else runFlashCycle $ Set.fold flash os nineAndNotFlashed

countFlashed :: OctopusMap (Int, Bool) -> (OctopusMap Int, Int)
countFlashed os =
  let newOctopi = Map.map (\(e, f) -> if f then 0 else e) os
      flashes = Map.size $ Map.filter snd os
   in (newOctopi, flashes)

flash :: (Int, Int) -> OctopusMap (Int, Bool) -> OctopusMap (Int, Bool)
flash flashing os =
  let flashed = Map.adjust (\(e, _) -> (e, True)) flashing os
      energized = foldr (Map.adjust (\(e, f) -> (e + 1, f))) flashed (neighbours flashing)
   in energized

-- >>> neighbours (5,5)
-- [(4,4),(4,5),(4,6),(5,4),(5,6),(6,4),(6,5),(6,6)]
neighbours :: (Int, Int) -> [(Int, Int)]
neighbours (x, y) =
  [ (x', y')
    | x' <- [x - 1, x, x + 1],
      y' <- [y - 1, y, y + 1],
      (x', y') /= (x, y)
  ]

type OctopusMap a = Map (Int, Int) a

readInput :: IO (OctopusMap Int)
readInput = do
  ls <- lines <$> getContents
  pure . Map.fromList . concat $
    zipWith
      ( \l i ->
          zipWith
            (\c j -> ((i, j), read [c]))
            l
            [0 ..]
      )
      ls
      [0 ..]
